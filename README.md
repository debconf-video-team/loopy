dependencies:

 - obs-advanced-scene-switcher
 - obs-gradient-source
 - obs-plugins 
 - obs-time-source (currently unstable only)

manual installations:

- obs-markdown (not used for loopy24, it's crashy)
- obs-scene-tree-view (not used for loopy24, plugin fails to load)
