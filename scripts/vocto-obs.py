#!/usr/bin/python3

import logging
import json
from asyncio import (
    CancelledError, create_task, get_running_loop, run, sleep, open_connection,
    wait_for)
from collections import defaultdict

log = logging.getLogger(__name__)


class Voctomix:
    def __init__(self, host):
        self.host = host
        self.control = VoctomixControl(host, self)
        self.reconnection_task = None

    async def connect(self, reconnect_on_error=False):
        """Connect to control, and optionally reconnect forever"""
        if reconnect_on_error:
            self.reconnection_task = create_task(self.maintain_connection())
        else:
            await self._connect()

    async def maintain_connection(self, timeout=5):
        while True:
            try:
                await wait_for(self._connect(), timeout)
                await self.control.disconnection
            except CancelledError:
                return
            except Exception as e:
                log.error("Failed to connect: %s", e)
            await sleep(5)
            await self._disconnect()

    async def _connect(self):
        await self.control.connect()

    async def disconnect(self):
        """Disconnect and stop any reconnection attempts"""
        if self.reconnection_task and not self.reconnection_task.done():
            self.reconnection_task.cancel()
        await self._disconnect()

    async def _disconnect(self):
        await self.control.disconnect()

    @property
    def state(self):
        return self.control.state

    def state_changed(self, state):
        loop_visible = (state.get('video_a') == 'loop')
                        #or state.get('video_b') == 'loop')
                        #and state.get('stream_status') == 'live')
        text_file = open("/home/videoteam/scene.obs", "w")
        if loop_visible:
            print("Switching to Loop")
            text_file.write("Loop")
        else:
            print("Switching to Standby")
            text_file.write("Standby")
        text_file.close()


class VoctomixControl:
    def __init__(self, host, voctomix):
        self.host = host
        # A Future that is pending as long as we're connected:
        self.disconnection = None
        self.loop = get_running_loop()
        # Our view of Voctocore's state:
        self.state = {}
        self.reader_task = None
        # Responses that we're .expect()ing from the core:
        self.response_futures = defaultdict(list)
        self.voctomix = voctomix

    async def connect(self):
        """Initialize state, and connect to voctocore"""
        log.info('Connecting to voctomix control')
        self._reader, self._writer = await open_connection(self.host, 9999)
        log.debug('Connected')
        self.disconnection = self.loop.create_future()
        self.state.clear()
        self.reader_task = create_task(self.reader())

        # Initialize our state
        await self.send('get_config')
        await self.send('get_audio')
        await self.send('get_stream_status')
        await self.send('get_composite_mode_and_video_status')
        self.state['connected'] = True

    async def disconnect(self, reason=None):
        """Disconnect from voctocore"""
        if not self.disconnection or self.disconnection.done():
            return
        log.warn('Disconnecting from voctomix control for %s', reason)
        self.state['connected'] = False
        for futures in self.response_futures.values():
            for future in futures:
                if not future.done():
                    future.cancel()
        self.response_futures.clear()
        if self.reader_task and not self.reader_task.done():
            self.reader_task.cancel()
        if not self._writer.is_closing():
            self._writer.close()
        self.disconnection.set_result(reason)

    async def reader(self):
        """Follow events from the core

        They can arrive at any time.
        If anyone is waiting for one of them, notify them.
        """
        while not self._reader.at_eof():
            try:
                line = await self._reader.readline()
            except (ConnectionResetError, CancelledError) as e:
                await self.disconnect(e)
                return
            line = line.decode('utf-8').strip()
            try:
                cmd, args = line.split(None, 1)
            except ValueError:
                continue
            self.update_state(cmd, args)
            futures = self.response_futures[cmd]
            while futures:
                future = futures.pop(0)
                future.set_result(args)
            self.voctomix.state_changed(self.state)

        await self.disconnect(EOFError)

    async def send(self, *command):
        """Send a command to voctomix"""
        cmd = ' '.join(command)
        self._writer.write(cmd.encode('utf-8'))
        self._writer.write(b'\n')
        await self._writer.drain()
        last_responses = {
            'get_audio': 'audio_status',
            'get_config': 'server_config',
            'get_composite_mode_and_video_status':
                'composite_mode_and_video_status',
            'get_stream_status': 'stream_status',
            'message': 'message',
            'set_audio_volume': 'audio_status',
            'set_composite_mode': 'composite_mode_and_video_status',
            'set_stream_blank': 'stream_status',
            'set_stream_live': 'stream_status',
            'set_stream_loop': 'stream_status',
            'set_video_a': 'video_status',
            'set_video_b': 'video_status',
        }
        return await self.expect(last_responses[command[0]])

    async def expect(self, command):
        """Wait for a particular response from voctomix"""
        future = self.loop.create_future()
        self.response_futures[command].append(future)
        return await future

    def update_state(self, cmd, args):
        """Update our view of Voctomix's state, based on a received message"""
        if cmd == 'server_config':
            self.config = json.loads(args)
            self.state['sources'] = self.config['mix']['sources'].split(',')
        elif cmd == 'audio_status':
            self.state['audio'] = json.loads(args)
        elif cmd == 'composite_mode_and_video_status':
            mode, a, b = args.split()
            self.state['video_a'] = a
            self.state['video_b'] = b
            self.state['composite_mode'] = mode
        elif cmd == 'video_status':
            a, b = args.split()
            self.state['video_a'] = a
            self.state['video_b'] = b
        elif cmd == 'stream_status':
            self.state['stream_status'] = args


async def main():
    voctomix = Voctomix(host='voctomix1.dc21.debconf.org')
    await voctomix.maintain_connection()


if __name__ == '__main__':
    run(main())
